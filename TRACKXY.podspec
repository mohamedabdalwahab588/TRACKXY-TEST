
Pod::Spec.new do |spec|

  spec.name           = "TRACKXY"
  spec.version        = "0.0.3"
  spec.summary        = "Track Your location Latitude and longitude"
  spec.description    = "using push protocol to get any update in permissions when calling framework object one time, and can use Push RXPermission View to show all permission."
  spec.homepage       = "https://github.com/Mohamed9195/TRACKXY-Test"
  spec.license        = "MIT"
  spec.author             = { "Mohamed Hashem" => "mohamedabdalwahab588@gmail.com" }
  spec.source         = { :git => "https://github.com/Mohamed9195/TRACKXY-Test.git", :tag => "#{spec.version}" }
  spec.platform       = :ios, "12.0"
  spec.ios.deployment_target = "12.0"
  spec.source_files   = 'TRACKXY'
  spec.source_files   =  'TRACKXY/**/*.swift'
  spec.exclude_files  = "Classes/Exclude"
  spec.resources      = 'Resources/Info.plist'
  spec.resources      = "CTYCrashReporter/Assets/*"

  spec.subspec 'App' do |app|
  app.source_files = 'TRACKXY/**/*.swift'
end

  spec.swift_version = "4.2"
  spec.dependency 'RxSwift'
  spec.dependency 'RxCocoa'
  spec.dependency 'Moya/RxSwift', '~> 14.0'


end
